require 'rails_helper'

RSpec.describe 'Api::V1::Infections' do
  describe 'POST /api/v1/infections' do
    let(:infected_user) { create(:user) }
    let(:infector_user) { create(:user) }

    let(:do_post) { post '/api/v1/infections', params: params, as: :json }

    context 'valid params' do
      let!(:params) do
        {
          infection:
          {
            infected_id: infected_user.id,
            infector_id: infector_user.id
          }
        }
      end
      
      it 'returns 201 http status' do
        do_post

        expect(response).to have_http_status(201)
      end

      it 'calls UserInfectorService' do
        expect(UserInfectorService).to receive(:call)

        do_post
      end

      it 'creates an InfectedUser' do
        expect { do_post }.to change(InfectedUser, :count).by(1)
      end
    end

    context 'invalid params' do
      let!(:params) do
        {
          infection:
          {
            infected_id: nil,
            infector_id: nil
          }
        }
      end

      it 'returns 400 http status' do
        do_post

        expect(response).to have_http_status(400)
      end

      it 'returns full error messages' do
        do_post

        expected_error_message = 'Users not found! Please consider only valid users.'

        expect(JSON.parse(response.body)['error']).to eq expected_error_message
      end

      it 'does not create an InfectedUser' do
        expect { do_post }.not_to change(InfectedUser, :count)
      end
    end
  end
end
