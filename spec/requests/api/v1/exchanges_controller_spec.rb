require 'rails_helper'

RSpec.describe 'Api::V1::Exchanges' do
  describe 'POST /api/v1/exchanges' do
    let(:do_post) { post '/api/v1/exchanges', params: params, as: :json }
    let(:controller_params) { ActionController::Parameters.new(params[:exchange]) }

    context 'valid exchange' do
      let!(:params) do
        {
          exchange: {
            user_a: {
              id: 1,
              items: ['food', 'medicine']
            },
            user_b: {
              id: 2,
              items: ['water', 'ammunition']
            }
          }
        }
      end

      it 'calls ExchangeService' do
        expect(ExchangeService).to receive(:call).with(controller_params).and_return(true)

        do_post
      end

      it 'returns 200 http status' do
        expect(ExchangeService).to receive(:call).with(controller_params).and_return(true)

        do_post

        expect(response).to have_http_status(200)
      end
    end

    context 'invalid exchange' do
      let!(:params) do
        {
          exchange: {
            user_a: {
              id: nil,
              items: []
            },
            user_b: {
              id: nil,
              items: []
            }
          }
        }
      end

      it 'calls ExchangeService' do
        expect(ExchangeService).to receive(:call).with(controller_params).and_return(false)

        do_post
      end

      it 'returns 400 http status' do
        expect(ExchangeService).to receive(:call).with(controller_params).and_return(false)

        do_post

        expect(response).to have_http_status(400)
      end

      it 'returns full error message' do
        expect(ExchangeService).to receive(:call).with(controller_params).and_return(false)

        do_post

        response_JSON = JSON.parse(response.body)
        expected_error_message = %q(Invalid exchange! Please consider only valid users, items owned by users and equivalent items points.)

        expect(response_JSON['error']).to eq expected_error_message
      end
    end
  end
end
