require 'rails_helper'

RSpec.describe 'Api::V1::Users' do
  describe 'POST /api/v1/users' do
    let!(:params) { { user: attributes_for(:user) } }
    let(:do_post) { post '/api/v1/users', params: params, as: :json }

    let(:expected_response) do
      {
        'age'      => 24,
        'infected' => false,
        'lat'      => nil,
        'lng'      => nil,
        'name'     => 'John',
        'sex'      => 'male'
       }
    end

    context 'valid params' do
      it 'returns 201 http status' do
        do_post

        expect(response).to have_http_status(201)
      end

      it 'returns user params info' do
        do_post

        response_JSON = JSON.parse(response.body).except!('id', 'created_at', 'updated_at')

        expect(response_JSON).to eq(expected_response)
      end

      it 'creates an User' do
        expect { do_post }.to change(User, :count).by(1)
      end

      it 'creates an Inventory' do
        expect { do_post }.to change(Inventory, :count).by(1)
      end

      it 'associates Inventory to User' do
        do_post

        expect(User.last.inventory).to be_a(Inventory)
      end
    end

    context 'invalid params' do
      let!(:params) do
        {
          user:
          {
            name: nil,
            sex: nil,
            age: nil
          }
        }
      end

      it 'returns 400 http status' do
        do_post

        expect(response).to have_http_status(400)
      end

      it 'returns full error messages' do
        do_post

        expected_error_message =
          %(Name can't be blank, Sex can't be blank, Sex is not included in the list, and Age can't be blank)

        expect(JSON.parse(response.body)['error']).to eq expected_error_message
      end

      it 'does not create an User' do
        expect { do_post }.not_to change(User, :count)
      end

      it 'does not create an Inventory' do
        expect { do_post }.not_to change(Inventory, :count)
      end
    end
  end

  describe 'PATCH api/v1/users/:id/update_location' do
    let(:do_patch) do
      patch "/api/v1/users/#{user.id}/update_location", params: params, as: :json
    end

    let(:user) { create(:user) }

    context 'valid params' do
      let(:params) do
        {
          user:
          {
            lat: -24.759900,
            lng: -42.327426
          }
        }
      end

      it 'returns 204 http status' do
        do_patch

        expect(response).to have_http_status(204)
      end

      it %(updates user's lat value) do
        old_lat = user.lat
        new_lat = params[:user][:lat]

        expect do
          do_patch
        end.to change { user.reload.lat }.from(old_lat).to(new_lat)
      end

      it %(updates user's lng value) do
        old_lng = user.lng
        new_lng = params[:user][:lng]

        expect do
          do_patch
        end.to change { user.reload.lng }.from(old_lng).to(new_lng)
      end
    end

    context 'invalid params' do
      let(:params) do
        {
          user:
          {
            lat: nil,
            lng: nil
          }
        }
      end

      it 'returns 400 http status' do
        do_patch

        expect(response).to have_http_status(400)
      end

      it 'returns full error messages' do
        do_patch

        expected_error_message = %(Lat can't be blank and Lng can't be blank)

        expect(JSON.parse(response.body)['error']).to eq expected_error_message
      end

      it %(keeps user's lat value) do
        expect { do_patch }.not_to change { user.reload.lat }
      end

      it %(keeps user's lng value) do
        expect { do_patch }.not_to change { user.reload.lng }
      end
    end
  end

  describe 'POST /api/v1/users/:id/add_items' do
    before do
      create(:water_item)
      create(:food_item)
      create(:medicine_item)
      create(:ammunition_item)
    end

    let(:inventory) { create(:inventory) }
    let(:user) { inventory.user }
    let(:do_post) { post "/api/v1/users/#{user.id}/add_items", params: params, as: :json }

    let(:params) do
      {
        items: ['water', 'food', 'medicine', 'ammunition']
      }
    end

    context 'valid params' do
      it 'calls InventoryItemsService' do
        expect(InventoryItemsService).to receive(:add_items)

        do_post
      end

      it 'returns 200 http status' do
        do_post

        expect(response).to have_http_status(200)
      end

      it 'adds all items to User' do
        expect { do_post }.to change(user.inventory_items, :count).by(4)
      end
    end

    context 'invalid params' do
      let(:params) do
        {
          items: ['truck', 'car']
        }
      end

      it 'returns full error message' do
        do_post

        expected_error_message =
          %(Invalid items list! Choose at least one of the following options: 'water', 'food', 'medicine', 'ammunition')

        expect(JSON.parse(response.body)['error']).to eq expected_error_message
      end
    end

    context 'none items' do
      let(:params) do
        {
          items: []
        }
      end

      it 'returns 400 http status' do
        do_post

        expect(response).to have_http_status(400)
      end

      it 'returns full error message' do
        do_post

        expected_error_message =
          %(Invalid items list! Choose at least one of the following options: 'water', 'food', 'medicine', 'ammunition')

        expect(JSON.parse(response.body)['error']).to eq expected_error_message
      end
    end
  end

  describe 'DELETE /api/v1/users/:id/remove_items' do
    before do
      item = create(:water_item)
      create(:food_item)
      create(:medicine_item)
      create(:ammunition_item)

      create(:inventory_item, inventory: inventory, item: item)
    end

    let!(:inventory) { create(:inventory) }
    let!(:user) { inventory.user }
    let(:do_delete) { delete "/api/v1/users/#{user.id}/remove_items", params: params, as: :json }

    let(:params) do
      {
        items: ['water', 'food', 'medicine', 'ammunition']
      }
    end

    context 'valid params' do
      it 'returns 200 http status' do
        do_delete

        expect(response).to have_http_status(200)
      end

      it 'removes only items that User owns' do
        expect { do_delete }.to change(user.inventory_items, :count).by(-1)
      end
    end

    context 'invalid params' do
      let(:params) do
        {
          items: ['truck', 'car']
        }
      end

      it 'returns full error message' do
        do_delete

        expected_error_message =
          %(User does not have these items or invalid items list! Choose at least one of the following options: 'water', 'food', 'medicine', 'ammunition')

        expect(JSON.parse(response.body)['error']).to eq expected_error_message
      end
    end

    context 'none items' do
      let(:params) do
        {
          items: []
        }
      end

      it 'returns 400 http status' do
        do_delete

        expect(response).to have_http_status(400)
      end

      it 'returns full error message' do
        do_delete

        expected_error_message =
          %(User does not have these items or invalid items list! Choose at least one of the following options: 'water', 'food', 'medicine', 'ammunition')

        expect(JSON.parse(response.body)['error']).to eq expected_error_message
      end
    end
  end
end
