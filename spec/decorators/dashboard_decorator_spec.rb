require 'rails_helper'

RSpec.describe DashboardDecorator do
  describe '.infected_users_percentage' do
    context 'when there are' do
      before do
        create_list(:user, 3, :infected)
        create_list(:user, 7)
      end

      it { expect(described_class.infected_users_percentage).to eq '30.0%' }
    end

    context 'when there arent' do
      it { expect(described_class.infected_users_percentage).to eq '0.0%' }
    end
  end

  describe '.not_infected_users_percentage' do
    context 'when there are' do
      before do
        create_list(:user, 3)
        create_list(:user, 7, :infected)
      end

      it { expect(described_class.not_infected_users_percentage).to eq '30.0%' }
    end

    context 'when there arent' do
      it { expect(described_class.not_infected_users_percentage).to eq '0.0%' }
    end
  end

  describe '.water_per_user_average' do
    context 'when there are' do
      before do
        water = create(:water_item)
        inventory = create(:inventory)

        create_list(:inventory, 2)
        create_list(:inventory_item, 9, inventory: inventory, item: water)
      end

      it { expect(described_class.water_per_user_average).to eq 3 }
    end

    context 'when there arent' do
      it { expect(described_class.water_per_user_average).to eq 0 }
    end
  end

  describe '.food_per_user_average' do
    context 'when there are' do
      before do
        food = create(:food_item)
        inventory = create(:inventory)

        create_list(:inventory, 2)
        create_list(:inventory_item, 9, inventory: inventory, item: food)
      end

      it { expect(described_class.food_per_user_average).to eq 3 }
    end

    context 'when there arent' do
      it { expect(described_class.food_per_user_average).to eq 0 }
    end
  end

  describe '.medicine_per_user_average' do
    context 'when there are' do
      before do
        medicine = create(:medicine_item)
        inventory = create(:inventory)

        create_list(:inventory, 2)
        create_list(:inventory_item, 9, inventory: inventory, item: medicine)
      end

      it { expect(described_class.medicine_per_user_average).to eq 3 }
    end

    context 'when there arent' do
      it { expect(described_class.medicine_per_user_average).to eq 0 }
    end
  end

  describe '.ammunition_per_user_average' do
    context 'when there are' do
      before do
        ammunition = create(:ammunition_item)
        inventory = create(:inventory)

        create_list(:inventory, 2)
        create_list(:inventory_item, 9, inventory: inventory, item: ammunition)
      end

      it { expect(described_class.ammunition_per_user_average).to eq 3 }
    end

    context 'when there arent' do
      it { expect(described_class.ammunition_per_user_average).to eq 0 }
    end
  end
end
