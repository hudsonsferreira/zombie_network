require 'rails_helper'

RSpec.describe UserInfectorService do
  describe '.call' do
    context 'with args' do
      let(:infected_user) { create(:user) }
      let(:infector_user) { create(:user) }

      it 'register an infection to infected_user' do
        expect(infected_user.infections.count).to be_zero

        described_class.call(infected_user, infector_user)

        expect(infected_user.reload.infections.count).to eq 1
      end

      context 'when infected user has three infections' do
        it 'sets infected_user#infected to true when infections are from diff infectors' do
          expect do
            described_class.call(infected_user, create(:user))
            described_class.call(infected_user, create(:user))
            described_class.call(infected_user, create(:user))
          end.to change { infected_user.reload.infected }.from(false).to(true)
        end

        it 'keeps infected_user#infected false when infections are from same infector' do
          expect do
            described_class.call(infected_user, infector_user)
            described_class.call(infected_user, infector_user)
            described_class.call(infected_user, infector_user)
          end.to_not change { infected_user.reload.infected }
        end

        it 'keeps infected_user#infected false when two infections are from same infector' do
          expect do
            described_class.call(infected_user, create(:user))
            described_class.call(infected_user, infector_user)
            described_class.call(infected_user, infector_user)
          end.to_not change { infected_user.reload.infected }
        end
      end
    end

    context 'without args' do
      it 'raises error' do
        expect { described_class.call }.to raise_error ArgumentError
      end
    end
  end
end
