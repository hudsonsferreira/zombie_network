require 'rails_helper'

RSpec.describe ExchangeService do
  describe '.call' do
    let(:inventory_a) { create(:inventory) }
    let(:inventory_b) { create(:inventory) }

    let(:user_a) { inventory_a.user }
    let(:user_b) { inventory_b.user }

    let(:water)      { create(:water_item) }
    let(:food)       { create(:food_item) }
    let(:medicine)   { create(:medicine_item) }
    let(:ammunition) { create(:ammunition_item) }

    context 'valid params' do
      context 'valid exchange' do
        let!(:params) do
          {
            exchange: {
              user_a: {
                id: user_a.id,
                items: ['food', 'medicine']
              },
              user_b: {
                id: user_b.id,
                items: ['water', 'ammunition']
              }
            }
          }
        end

        before do
          create(:inventory_item, inventory: inventory_a, item: food)
          create(:inventory_item, inventory: inventory_a, item: medicine)

          create(:inventory_item, inventory: inventory_b, item: water)
          create(:inventory_item, inventory: inventory_b, item: ammunition)
        end

        it 'exchanges items between users' do
          described_class.call(params[:exchange])

          expect(inventory_a.items).to include(water, ammunition)
          expect(inventory_a.items).not_to include(food, medicine)

          expect(inventory_b.items).to include(food, medicine)
          expect(inventory_b.items).not_to include(water, ammunition)
        end
      end

      context 'user_b does not have all items' do
        let!(:params) do
          {
            exchange: {
              user_a: {
                id: user_a.id,
                items: [food.name, medicine.name]
              },
              user_b: {
                id: user_b.id,
                items: [water.name, ammunition.name]
              }
            }
          }
        end

        before do
          create(:inventory_item, inventory: inventory_a, item: food)
          create(:inventory_item, inventory: inventory_a, item: medicine)

          create(:inventory_item, inventory: inventory_b, item: water)
        end

        it 'returns false' do
          expect(described_class.call(params[:exchange])).to be_falsey
        end
      end

      context %q(when items points are not equivalent)  do
        let!(:params) do
          {
            exchange: {
              user_a: {
                id: user_a.id,
                items: [food.name, medicine.name, food.name]
              },
              user_b: {
                id: user_b.id,
                items: [water.name, ammunition.name]
              }
            }
          }
        end

        before do
          create(:inventory_item, inventory: inventory_a, item: food)
          create(:inventory_item, inventory: inventory_a, item: food)
          create(:inventory_item, inventory: inventory_a, item: medicine)

          create(:inventory_item, inventory: inventory_b, item: water)
          create(:inventory_item, inventory: inventory_b, item: ammunition)
        end

        it 'returns false' do
          expect(described_class.call(params[:exchange])).to be_falsey
        end
      end
    end

    context 'invalid params' do
      let!(:params) do
        {
          exchange: {
            user_a: {
              id: nil,
              items: []
            },
            user_b: {
              id: nil,
              items: []
            }
          }
        }
      end

      it 'returns false' do
        expect(described_class.call(params[:exchange])).to be_falsey
      end
    end

    context 'without params' do
      it 'raises error' do
        expect { described_class.call }.to raise_error ArgumentError
      end
    end
  end
end
