require 'rails_helper'

RSpec.describe ExchangeItemCalculator do
  describe '.call' do
    context 'with args' do
      context 'when items list has same points sum' do
        let(:items_list) do
          [
            create(:water_item),
            create(:food_item),
            create(:ammunition_item)
          ]
        end

        let(:another_items_list) do
          [
            create(:water_item),
            create(:medicine_item),
            create(:ammunition_item),
            create(:ammunition_item)
          ]
        end

        it 'returns true' do
          expect(described_class.call(items_list, another_items_list)).to be_truthy
        end
      end

      context 'when items list has diff points sum' do
        let(:items_list) do
          [
            create(:water_item),
            create(:food_item),
            create(:ammunition_item)
          ]
        end

        let(:another_items_list) do
          [
            create(:water_item),
            create(:medicine_item)
          ]
        end

        it 'returns false' do
          expect(described_class.call(items_list, another_items_list)).to be_falsey
        end
      end
    end

    context 'without args' do
      it 'raises error' do
        expect { described_class.call }.to raise_error ArgumentError
      end
    end
  end
end
