require 'rails_helper'

RSpec.describe InventoryItemsService do
  describe '.add_items' do
    context 'with args' do
      before do
        create(:water_item)
        create(:food_item)
        create(:medicine_item)
        create(:ammunition_item)
      end

      let(:inventory) { create(:inventory) }

      let(:items_name_list) do
        ['water', 'food', 'medicine', 'ammunition']
      end

      it 'add items to inventory' do
        described_class.add_items(inventory, items_name_list)

        expect(inventory.inventory_items.count).to eq 4
      end
    end

    context 'without args' do
      it 'raises error' do
        expect { described_class.add_items }.to raise_error ArgumentError
      end
    end
  end
end
