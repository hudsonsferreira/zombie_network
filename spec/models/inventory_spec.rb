require 'rails_helper'

RSpec.describe Inventory do
  describe 'associations' do
    it { is_expected.to belong_to :user }
    it { is_expected.to have_many(:inventory_items).dependent(:destroy) }
    it { is_expected.to have_many(:items).through(:inventory_items) }
  end

  describe '#has_inventory_items?' do
    context 'when it has' do
      let(:inventory) { create(:inventory) }
      let(:item) { create(:water_item) }
      let(:inventory_item) { create(:inventory_item, inventory: inventory, item: item) }

      it do
        expect(inventory.has_inventory_items?('water')).to match_array [inventory_item]
      end
    end

    context 'when it has not' do
      it do
        expect(described_class.new.has_inventory_items?(['food'])).to be_empty
      end
    end
  end
end
