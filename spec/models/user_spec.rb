require 'rails_helper'

RSpec.describe User do
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :age }
    it { is_expected.to validate_presence_of :sex }
    it { is_expected.to validate_inclusion_of(:sex).in_array(described_class::SEXES) }
    it { is_expected.to validate_presence_of(:lat).on(:update_location) }
    it { is_expected.to validate_presence_of(:lng).on(:update_location) }
  end

  describe 'associations' do
    it { is_expected.to have_one(:inventory).dependent(:destroy) }
    it { is_expected.to have_many(:inventory_items).through(:inventory) }
    it do
      is_expected.to have_many(:infections).class_name(InfectedUser)
        .with_foreign_key('infected_id').dependent(:destroy)
    end
  end

  describe 'scopes' do
    describe 'default scope' do
      it 'returns only not infected Users' do
        not_infected = create(:user)
        another_not_infected = create(:user)
        create(:user, :infected)

        expect(described_class.all).to match_array [another_not_infected, not_infected]
      end
    end

    describe '.infected' do
      it 'returns only infected Users' do
        create(:user)
        infected = create(:user, :infected)

        expect(described_class.infected).to match_array [infected]
      end
    end
  end
end
