require 'rails_helper'

RSpec.describe InventoryItem do
  describe 'associations' do
    it { is_expected.to belong_to :inventory }
    it { is_expected.to belong_to :item }
  end
end
