require 'rails_helper'

RSpec.describe Item do
  describe 'validations' do
    it { is_expected.to validate_presence_of :name }
    it { is_expected.to validate_presence_of :points }
    it { is_expected.to validate_inclusion_of(:name).in_array(described_class::NAMES) }
  end

  describe 'associations' do
    it { is_expected.to have_many(:inventory_items).dependent(:destroy) }
  end

  describe '.valid_items?' do
    context 'when valid' do
      it 'returns true' do
        items_list = ['water']

        expect(described_class.valid_items?(items_list)).to be_truthy
      end
      it 'returns true' do
        items_list = ['water', 'ammunition']

        expect(described_class.valid_items?(items_list)).to be_truthy
      end

      it 'returns true' do
        items_list = ['water', 'medicine', 'food', 'ammunition']

        expect(described_class.valid_items?(items_list)).to be_truthy
      end

      it 'returns true' do
        items_list = ['water', 'medicine']

        expect(described_class.valid_items?(items_list)).to be_truthy
      end
    end

    context 'when invalid' do
      it 'returns false' do
        items_list = ['cake', 'rice']

        expect(described_class.valid_items?(items_list)).to be_falsey
      end
    end
  end
end
