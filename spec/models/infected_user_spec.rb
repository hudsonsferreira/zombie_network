require 'rails_helper'

RSpec.describe InfectedUser do
  describe 'associations' do
    it { is_expected.to belong_to(:infected).class_name(User) }
    it { is_expected.to belong_to(:infector).class_name(User) }
  end

  describe 'scopes' do
    describe '.with_distinct_infectors' do
      let(:infected) { create(:user) }
      let(:infector) { create(:user) }
      let(:another_infector) { create(:user) }

      let!(:infection_with_repeated_infector_id) { create(:infection, infected: infected, infector: infector) }
      let!(:another_distinct_infection) { create(:infection, infected: infected, infector: another_infector) }
      let!(:distinct_infection) { create(:infection, infected: infected, infector: infector) }

      let(:expected_two_infectors_ids) { [infector.id, another_infector.id] }

      it 'returns only distinct infections by infectors' do
        expect(described_class.with_distinct_infectors.map(&:infector_id)).to match_array expected_two_infectors_ids
      end
    end
  end
end
