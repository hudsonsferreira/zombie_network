FactoryGirl.define do
  factory :user do
    name 'John'
    age 24
    sex 'male'
    infected false

    trait :infected do
      infected true
    end
  end
end
