FactoryGirl.define do
  factory :inventory_item do
    inventory
    item
  end
end
