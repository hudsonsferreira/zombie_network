FactoryGirl.define do
  factory :water_item, class: :Item, aliases: [:item] do
    name 'water'
    points 4
  end

  factory :food_item, class: :Item do
    name 'food'
    points 3
  end

  factory :medicine_item, class: :Item do
    name 'medicine'
    points 2
  end

  factory :ammunition_item, class: :Item do
    name 'ammunition'
    points 1
  end
end
