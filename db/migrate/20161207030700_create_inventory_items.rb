class CreateInventoryItems < ActiveRecord::Migration[5.0]
  def change
    create_table :inventory_items do |t|
      t.references :inventory, null: false
      t.references :item, null: false

      t.timestamps
    end
  end
end
