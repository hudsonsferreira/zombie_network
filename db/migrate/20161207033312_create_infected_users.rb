class CreateInfectedUsers < ActiveRecord::Migration[5.0]
  def change
    create_table :infected_users do |t|
      t.references :infected, null: false
      t.references :infector, null: false

      t.timestamps
    end
  end
end
