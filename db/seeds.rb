def self.create_inventory_items(inventory)
  water      = Item.find_or_create_by(name: 'water', points: 4)
  food       = Item.find_or_create_by(name: 'food', points: 3)
  medicine   = Item.find_or_create_by(name: 'medicine', points: 2)
  ammunition = Item.find_or_create_by(name: 'ammunition', points: 1)

  5.times do
    [water, food, medicine, ammunition].each do |item|
      InventoryItem.create!(inventory: inventory, item: item)
    end
  end
end

5.times do
  user = User.new(name: 'John', age: 32, sex: 'male')
  user.build_inventory
  user.save!

  self.create_inventory_items(user.inventory)
end

5.times do
  user = User.new(name: 'Ellen', age: 24, sex: 'female', infected: true)
  user.build_inventory
  user.save!

  self.create_inventory_items(user.inventory)
end
