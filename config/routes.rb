Rails.application.routes.draw do
  ActiveAdmin.routes(self)
  namespace :api do
    namespace :v1 do
      resources :users, only: :create do
        member do
          patch :update_location
          post :add_items
          delete :remove_items
        end
      end

      resources :infections, only: :create
      resources :exchanges,  only: :create
    end
  end

  root to: 'admin/dashboard#index'
end
