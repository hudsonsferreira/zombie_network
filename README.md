# README

Zombie Network is a Rails API to help humans to survive at the apocalypse zumbi.

## Dependencies
 * Ruby 2.3.1 - You can use [rbenv](https://github.com/rbenv/rbenv)
 * [PostgreSQL](http://www.postgresql.org/)
  * OSX - [Postgress.app](http://postgresapp.com/)
  * Linux - `$ sudo apt-get install postgresql`
  * Windows - [PostgreSQL for Windows](http://www.postgresql.org/download/windows/)

## Setup

`$ git clone git@bitbucket.org:hudsonsferreira/zombie_network.git`

`$ rails db:setup`

## Running server

`$ rails s`

### Accessing reports statistics

`http://localhost:3000`

## Running specs

`$ coverage=on rspec`

## API Documentation

On private APIs I don't think it's extremally necessary to use an API Documentation service.
Instead of it, I prefer to export my requests using [Insomnia](https://insomnia.rest/) to keep the docs easier to update.
With Insomnia it's possible to export the URL, HTTP method and parameters.
So, who is trying to understand how the API works just needs to import on Insomia the `docs/api/zombie_network_api.json`.

## Technical Decisions
 * `User` model has a default_scope to not infected users. All tries to find an existing User on APP is only considering not infected ones. To also consider infected users you need to use `User.unscoped`
 * All `Items` are associate directly with User's `Inventory`.
 * `UserInfectorService` is responsible to register user infections and become an user infected when he completes 3 infections registered.
 * `InventoryItemsService.add_items` is responsible to add items to User's inventory.
 * The action of remove inventory items is done directly on `UsersController` because it's very simple.
 * `ExchangeItemCalculator` is responsible to ensure if items on exchange have equivalent points.
 * `ExchangeService` is responsible to validate and realize the exchange.
 * Reports statistics are being showed through `DashboardDecorator` on `ActiveAdmin`.
