class InfectedUser < ApplicationRecord
  belongs_to :infected, class_name: User
  belongs_to :infector, class_name: User

  scope :with_distinct_infectors, -> { select(:infector_id).distinct }
end
