class Item < ApplicationRecord
  NAMES = %w(water food medicine ammunition).freeze

  extend Enumerize

  enumerize :name, in: NAMES

  validates :name, :points, presence: true
  validates :name, inclusion: { in: NAMES }

  has_many :inventory_items, dependent: :destroy

  def self.valid_items?(item_names_list)
    (item_names_list - Item::NAMES).empty?
  end
end
