class Inventory < ApplicationRecord
  belongs_to :user
  has_many :inventory_items, dependent: :destroy
  has_many :items, through: :inventory_items

  def has_inventory_items?(items_name_list)
    inventory_items
      .joins(:item).where('items.name in (?)', items_name_list)
  end
end
