class User < ApplicationRecord
  default_scope { where(infected: false) }

  SEXES = %w(male female).freeze

  extend Enumerize

  enumerize :sex, in: SEXES

  validates :name, :sex, :age, presence: true
  validates :sex, inclusion: { in: SEXES }
  validates :lat, :lng, presence: true, on: :update_location

  has_one :inventory, dependent: :destroy
  has_many :infections, class_name: InfectedUser,
           foreign_key: 'infected_id', dependent: :destroy

  has_many :inventory_items, through: :inventory

  scope :infected, -> { unscoped.where(infected: true)  }
end
