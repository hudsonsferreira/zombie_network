class UserInfectorService
  attr_reader :infected_user, :infector_user

  def self.call(infected_user, infector_user)
    new(infected_user, infector_user).call
  end

  def initialize(infected_user, infector_user)
    @infected_user, @infector_user = infected_user, infector_user
  end

  def call
    create_infection
    update_infected_user_infected_value if infected_user_has_three_diff_infections?
  end

  private

  def create_infection
    InfectedUser.create!(infected: infected_user, infector: infector_user)
  end

  def update_infected_user_infected_value
    infected_user.update!(infected: true)
  end

  def infected_user_has_three_diff_infections?
    infected_user.infections.with_distinct_infectors.count == 3
  end
end
