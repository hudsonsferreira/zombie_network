class ExchangeService
  attr_reader :params

  def self.call(params)
    new(params).call
  end

  def initialize(params)
    @params = params
    set_users
    set_items_name_list
    set_inventories
    set_items
  end

  def call
    exchange_items! if valid_exchange?
  end

  private

  def valid_exchange?
    all_objects_exists? && items_have_equivalent_points?
  end

  def set_users
    @user_a = User.find_by(id: params[:user_a][:id])
    @user_b = User.find_by(id: params[:user_b][:id])
  end

  def set_items_name_list
    @items_name_list_a = params[:user_a][:items]
    @items_name_list_b = params[:user_b][:items]
  end

  def set_inventories
    @inventory_a, @inventory_b = @user_a.try(:inventory), @user_b.try(:inventory)
  end

  def set_items
    @items_a = @inventory_a.items.where(name: @items_name_list_a).limit(@items_name_list_a.count) if @inventory_a
    @items_b = @inventory_b.items.where(name: @items_name_list_b).limit(@items_name_list_b.count) if @inventory_b
  end

  def all_objects_exists?
     users_exists? && items_exists?
  end

  def users_exists?
    @user_a.present? && @user_b.present?
  end

  def items_exists?
    @items_a.present? && @items_b.present?
  end

  def items_have_equivalent_points?
    ExchangeItemCalculator.call(@items_a, @items_b)
  end

  def exchange_items!
    @inventory_a.items.delete(@items_a)
    @inventory_a.items << @items_b

    @inventory_b.items.delete(@items_b)
    @inventory_b.items << @items_a
  end
end
