class InventoryItemsService
  attr_reader :inventory, :items_name_list

  def self.add_items(inventory, items_name_list)
    new(inventory, items_name_list).add_items
  end

  def initialize(inventory, items_name_list)
    @inventory, @items_name_list = inventory, items_name_list
  end

  def add_items
    items_name_list.each do |item_name|
      item = Item.find_by(name: item_name)
      InventoryItem.create!(inventory: inventory, item: item)
    end
  end
end
