class ExchangeItemCalculator
  attr_reader :first_items_list, :second_items_list

  def self.call(first_items_list, second_items_list)
    new(first_items_list, second_items_list).call
  end

  def initialize(first_items_list, second_items_list)
    @first_items_list, @second_items_list = first_items_list, second_items_list
  end

  def call
    calc_items_points(first_items_list) == calc_items_points(second_items_list)
  end

  private

  def calc_items_points(items)
    items.sum(&:points)
  end
end
