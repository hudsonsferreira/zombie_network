module Api
  module V1
    class UsersController < BaseController
      before_action :set_user, only: :update_location
      before_action :set_user_inventory, :set_items, only: [:add_items, :remove_items]
      before_action :set_inventory_items, only: :remove_items

      def create
        user = User.new(user_params)
        user.build_inventory

        if user.save
          render json: user, status: :created
        else
          render json: { error: user.errors.full_messages.to_sentence }, status: :bad_request
        end
      end

      def update_location
        if @user.update_attributes(user_params) && @user.valid?(:update_location)
          render status: :no_content
        else
          render json: { error: @user.errors.full_messages.to_sentence }, status: :bad_request
        end
      end

      def add_items
        if valid_items?
          InventoryItemsService.add_items(@user_inventory, @items)
          render status: :ok
        else
          render json: {
            error: I18n.t('users.add_items.items_list_error')
          }, status: :bad_request
        end
      end

      def remove_items
        if @inventory_items.present?
          @inventory_items.destroy_all
          render status: :ok
        else
          render json: {
            error: I18n.t('users.remove_items.items_list_error')
          }, status: :bad_request
        end
      end

      private

      def user_params
        params.require(:user).permit(:name, :age, :sex,:lat, :lng)
      end

      def set_user
        @user = User.find(params[:id])
      end

      def set_user_inventory
        @user_inventory = User.find(params[:id]).inventory
      end

      def set_items
        @items = params[:items]
      end

      def valid_items?
        @items.present? && Item.valid_items?(@items)
      end

      def set_inventory_items
        @inventory_items = @user_inventory.has_inventory_items?(@items)
      end
    end
  end
end
