module Api
  module V1
    class ExchangesController < BaseController
      def create
        if ExchangeService.call(params[:exchange])
          render status: :ok
        else
          render json: {
            error: I18n.t('exchanges.create.invalid_exchange_error')
          }, status: :bad_request
        end
      end
    end
  end
end
