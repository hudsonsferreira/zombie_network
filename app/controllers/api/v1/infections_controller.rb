module Api
  module V1
    class InfectionsController < BaseController
      before_action :set_infected_user, :set_infector_user, only: :create

      def create
        if @infected_user.present? && @infector_user.present?
          UserInfectorService.call(@infected_user, @infector_user)

          render status: :created
        else
          render json: { error: I18n.t('infections.create.users_not_found_error') }, status: :bad_request
        end
      end

      private

      def set_infected_user
        @infected_user = User.find_by(id: params[:infection][:infected_id])
      end

      def set_infector_user
        @infector_user = User.find_by(id: params[:infection][:infector_id])
      end
    end
  end
end
