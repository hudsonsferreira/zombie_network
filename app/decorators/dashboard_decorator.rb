class DashboardDecorator
  class << self
    def infected_users_percentage
      return '0.0%' unless User.unscoped.count > 0

      infected = User.infected.count
      all = User.unscoped.count

      percentage_of(infected, all)
    end

    def not_infected_users_percentage
      return '0.0%' unless User.unscoped.count > 0

      not_infected = User.all.count
      all = User.unscoped.count

      percentage_of(not_infected, all)
    end

    def water_per_user_average
      waters_count = items_count('water')

      return 0 unless waters_count > 0

      waters_count / inventories_count.to_f
    end

    def food_per_user_average
      foods_count = items_count('food')

      return 0 unless foods_count > 0

      foods_count / inventories_count.to_f
    end

    def medicine_per_user_average
      medicines_count = items_count('medicine')

      return 0 unless medicines_count > 0

      medicines_count / inventories_count.to_f
    end

    def ammunition_per_user_average
      ammunitions_count = items_count('ammunition')

      return 0 unless ammunitions_count > 0

      ammunitions_count / inventories_count.to_f
    end

    private

    def percentage_of(x, y)
      "#{ ((x.to_f / y.to_f) * 100).round(1) }%"
    end

    def items_count(item_name)
      item = Item.find_by(name: item_name)
      return 0 unless item
      InventoryItem.where(item_id: item.id).count
    end

    def inventories_count
      Inventory.all.count
    end
  end
end
