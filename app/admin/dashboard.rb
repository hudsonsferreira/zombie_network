ActiveAdmin.register_page "Dashboard" do

  menu priority: 1, label: proc{ I18n.t("active_admin.dashboard") }

  content title: proc{ I18n.t("active_admin.dashboard") } do
    columns do
      column do
        panel 'General Statistics' do
          table_for nil do
            column('Infected Users') { DashboardDecorator.infected_users_percentage }
            column('Not Infected Users') { DashboardDecorator.infected_users_percentage }
            column('Food items per User') { DashboardDecorator.food_per_user_average }
            column('Water items per User') { DashboardDecorator.water_per_user_average }
            column('Medicine items per User') { DashboardDecorator.medicine_per_user_average }
            column('Ammunition items per User') { DashboardDecorator.ammunition_per_user_average }
          end
        end
      end
    end
  end
end
